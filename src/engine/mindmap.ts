import * as raw from "./raw_data.js"

//------------------------------------------------------------------------------
// Cooked types
//------------------------------------------------------------------------------

export interface Info {
    readonly kind: "info",
    text: string,
    tags: string[],
}
export interface Link {
    readonly kind: "link",
    text: string,
    tags: string[],
    target: Entity,
}

export type Value = Info | Link;
export type ValueList = Value[];

export type PropertyBag = Map<string, ValueList>;

export interface BackLink {
    readonly kind: "backlink";
    text: string,
    tags: string[],
    target: Entity,
}

export type BackLinkList = BackLink[];
export type BackLinkBag = Map<string, BackLinkList>;

export interface Entity {
    readonly id: string;
    readonly raw: raw.Entity;
    readonly properties: PropertyBag;
    readonly backLinks: BackLinkBag;
}

export type Index = Map<string, Entity>;
export type MultiIndex = Map<string, Entity[]>;

export interface Data {
    readonly byId: Index;
    readonly byProperty: Map<string, MultiIndex>;
}

//------------------------------------------------------------------------------
// Factories

function makeEntity(raw: raw.Entity): Entity {
    return {
        id: raw.id,
        raw: raw,
        properties: new Map(),
        backLinks: new Map(),
    }
}

function makeValue(v: string, ts: string[]): Info
function makeValue(v: string, ts: string[], t: Entity): Link
function makeValue(v: string, ts: string[], t?: Entity): Value
function makeValue(v: string, ts: string[], t?: Entity): Value {
    if (t) {
        return { kind: "link", text: v, tags: ts, target: t };
    } else {
        return { kind: "info", text: v, tags: ts };
    }
}

function makeBackLink(v: string, ts: string[], t: Entity): BackLink {
    return { kind: "backlink", text: v, tags: ts, target: t };
}

//------------------------------------------------------------------------------
// Conversion

export function buildData(rawData: raw.Data): Data {
    const cooked: Data = {
        byId: new Map(),
        byProperty: new Map(),
    };

    // create all the entities
    for (const rawEntityOrSublist of rawData) {
        if (rawEntityOrSublist instanceof Array) {
            const sublist = rawEntityOrSublist;
            for (const re of sublist) {
                const e = makeEntity(re);
                cooked.byId.set(e.id, e);
            }
        } else {
            const rawEntity = rawEntityOrSublist;
            const e = makeEntity(rawEntity);
            cooked.byId.set(e.id, e);
        }
    }

    // build links
    for (const [id, e] of cooked.byId) {
        for (const pn of Object.keys(e.raw)) {
            if (pn === "id") {
                continue;
            }

            if (!cooked.byProperty.get(pn)) {
                cooked.byProperty.set(pn, new Map());
            }

            const pv = e.raw[pn];
            if (typeof pv === "boolean") {
                addFlag(e, pn, cooked);
            } else if (typeof pv === "string") {
                addValue(e, pn, pv, [], cooked);
            } else if (pv instanceof Array) {
                for (const v of pv) {
                    addValue(e, pn, v, [], cooked);
                }
                if (pv.length === 0) {
                    addFlag(e, pn, cooked);
                }
            } else if (typeof pv === "object") {
                const keys = Object.keys(pv);
                for (const k of keys) {
                    const rts = pv[k]!;

                    const ts: string[] = [];
                    if (rts === true) {
                        // nothing to do
                    } else if (typeof rts === "string") {
                        ts.push(rts);
                    } else {
                        ts.concat(rts);
                    }

                    addValue(e, pn, k, ts, cooked);
                }
                if (keys.length === 0) {
                    addFlag(e, pn, cooked);
                }
            } else {
                throw new Error(`Invalid property '${id}.${pn}' = '${pv}' (${typeof pv})`);
            }
        }
    }

    return cooked;
}

export function addFlag(e: Entity, name: string, data: Data): void {
    e.properties.set(name, []);

    addToIndex(e, name, String(true), data);
}

function addValue(e: Entity, name: string, value: string, tags: string [], data: Data): void {
    if (value === "") {
        addFlag(e, name, data);
        return;
    }

    const target = data.byId.get(value);

    let values = e.properties.get(name);
    if (!values) {
        values = [];
        e.properties.set(name, values);
    }
    values.push(makeValue(value, tags, target));

    if (target) {
        // add the backlink
        let bl = target.backLinks.get(name);
        if (!bl) {
            bl = [];
            target.backLinks.set(name, bl);
        }
        bl.push(makeBackLink(e.id, tags, e));
    }

    addToIndex(e, name, value, data);
}

function addToIndex(e: Entity, name: string, value: string, data: Data): void {
    let i = data.byProperty.get(name);
    if (!i) {
        i = new Map();
        data.byProperty.set(name, i);
    }

    let c = i.get(value);
    if (!c) {
        c = [];
        i.set(value, c);
    }

    c.push(e);
}

export function removeProperty(e: Entity, name: string, data: Data): void {
    const oldValues = e.properties.get(name);
    if (!oldValues) {
        return;
    }

    e.properties.delete(name);

    for (const v of oldValues) {
        if (v.kind === "link") {
            removeBacklink(v.target, name, e.id, data);
        }
        removeFromIndex(e, name, v.text, data);
    }
}

function removeFromIndex(e: Entity, name: string, value: string, data: Data): void {
    const i = data.byProperty.get(name)!;
    const c = i.get(value)!;

    c.splice(c.indexOf(e), 1);
}

function removeBacklink(e: Entity, name: string, sourceId: string, data: Data): void {
    const sources = e.backLinks.get(name)!;
    for (let i = 0; i < sources.length; ++i) {
        const l = sources[i]!;
        if (l.text === sourceId) {
            sources.splice(i, 1);
            break;
        }
    }
}

//------------------------------------------------------------------------------
// Utils
//------------------------------------------------------------------------------

function assertNever(x: never): never {
    throw new Error("Unexpected object: " + x);
}

import * as mm from "./mindmap.js";

export interface Id {
    readonly kind: "id";
    id: string;
}

export function makeId(id: string): Id {
    return { kind: "id", id: id };
}

export interface Filter {
    readonly kind: "filter";
    (e: mm.Entity): boolean;
}

export interface ParseError {
    kind: "error";
    message: string;
    source: Span;
}

function makeError(message: string, source: Span): ParseError {
    return { kind: "error", message: message, source: source };
}

function isError(e: ParseObject | TokenizeResult): e is ParseError {
    return e.kind === "error";
}

export type ParseResult = Id | Filter | ParseError;

export interface Span {
    begin: number;
    end: number;
}

export function makeSpan(begin: number, end: number): Span {
    return { begin: begin, end: end };
}

function merge(l: Span, r: Span): Span {
    return { begin: Math.min(l.begin, r.begin), end: Math.max(l.end, r.end) };
}

export function search(data: mm.Data, f: Id | Filter): mm.Entity[] {
    const r: mm.Entity[] = [];

    if (f.kind == "id") {
        const e = data.byId.get(f.id);
        if (e) {
            r.push(e);
        }
    } else {
        for (const [_, e] of data.byId) {
            if (f(e)) {
                r.push(e);
            }
        }
    }

    return r;
}

export function parse(input: string): ParseResult {
    const tok = tokenize(input);
    if (isError(tok)) {
        return tok;
    }

    const ast = parseToAst(tok.tokens);
    if (isError(ast)) {
        return ast;
    }

    // special case "id = x" into an id filter
    if (ast.kind === "linkOp" && ast.operator === "=" && ast.property === "id") {
        return makeId(ast.target);
    }

    return buildFilter(ast);
}

interface ParseObject {
    kind: string;
    source: Span;
}

interface TokenizeState {
    data: string;
    offset: number;
}

type Operator = "=" | "!=" | "~" | "!~" | ">" | "!>" | "<" | "!<" | "+" | "-" | "&" | "|" | "(" | ")";
    // TODO ideas
    // p+>/p->/p+</p-< - p+/p- variants for link and backlink
    // drop unary operators and add a wildcard value for t?
    // how to do tag inspection

interface StringToken {
    kind: "string";
    value: string;
    source: Span;

}

interface OperatorToken {
    kind: "operator";
    value: Operator;
    source: Span;
}

interface WildcardToken {
    kind: "wildcard";
    source: Span;
}

type Token = StringToken | WildcardToken | OperatorToken;

interface Tokens {
    kind: "tokens";
    tokens: Token[];
}

type TokenizeResult = Tokens | ParseError;

function tokenize(input: string): TokenizeResult {
    const state = { data: input, offset: 0 };
    const ts: Token[] = [];

    while (true) {
        consumeWhitespace(state);

        if (state.offset === state.data.length) {
            break;
        }

        const o = tokenizeOperator(state);
        if (o) {
            ts.push(o);
            continue;
        }

        const s = tokenizeString(state);
        if (s) {
            ts.push(s);
            continue;
        }

        // the remained of the input could not be tokenized
        return makeError(`unrecognized token "${state.data.substr(state.offset)}"`, makeSpan(state.offset, state.data.length));
    }

    return { kind: "tokens", tokens: ts };
}

// TODO explore negative matching on the operator regex
// TODO consider using && and || for logical operators so & can be in strings
const g_reg_string = /[^=!~><+\-&|()]*/y;
function tokenizeString(state: TokenizeState): StringToken | null {
    const m = doMatch(state, g_reg_string);
    if (!m) {
        return null;
    } else if (!m[0][0]) {
        throw new Error(`Failed to tokenize ${state}`);
    }

    return { kind: "string", value: m[0][0].trim(), source: m[1] };
}

const g_reg_wildcard = /\*/y;
function tokenizeWildcard(state: TokenizeState): WildcardToken | null {
    const m = doMatch(state, g_reg_operator);
    if (!m) {
        return null;
    }
    return { kind: "wildcard", source: m[1] };
}

const g_reg_operator = /=|!=|~|!~|>|!>|<|!<|\+|-|&|\||\(|\)/y;
function tokenizeOperator(state: TokenizeState): OperatorToken | null {
    const m = doMatch(state, g_reg_operator);
    if (!m) {
        return null;
    } else if (!m[0][0]) {
        throw new Error(`Failed to tokenize ${state}`);
    }

    return { kind: "operator", value: m[0][0] as Operator, source: m[1] };
}

const g_reg_whitespace = /\s*/y;
function consumeWhitespace(state: TokenizeState): void {
    doMatch(state, g_reg_whitespace);
}

function doMatch(state: TokenizeState, regExp: RegExp): [RegExpMatchArray, Span] | null {
    const begin = state.offset;
    regExp.lastIndex = state.offset;

    const m = state.data.match(regExp);

    if (!m) { // abort before reading lastIndex, cause it will be 0 if no match was found
        return null;
    }

    state.offset = regExp.lastIndex;
    const end = state.offset;

    return [m, makeSpan(begin, end)];
}

interface ParseState {
    data: Token[];
    offset: number;
}

interface LinkOpNode {
    kind: "linkOp";
    operator: "=" | "!=" | "~" | "!~" | ">" | "!>" | "<" | "!<";
    property: string;
    target: string;
    source: Span;
}

interface FlagOpNode {
    kind: "flagOp";
    operator: "+" | "-";
    property: string;
    source: Span;
}

interface ExprOpNode {
    kind: "exprOp";
    operator: "&" | "|";
    left: Expression;
    right: Expression;
    source: Span;
}

type Expression = LinkOpNode | FlagOpNode | ExprOpNode;

type AstResult = Expression | ParseError;

function parseToAst(tokens: Token[]): AstResult {
    const state = { data: tokens, offset: 0 };
    const ast = parseExpression(state);
    if (state.offset < state.data.length) {
        const t = state.data[state.offset]!;
        return makeError(`unexpected token ${JSON.stringify(t)}`, t.source);
    }
    return ast;
}

function parseExpression(state: ParseState): AstResult {
    const sub0 = parseAtomicExpression(state);
    if (isError(sub0)) {
        return sub0;
    }

    const t1 = peek(state);
    if (!t1) {
        return sub0; // it's ok input can end here
    } else if (t1.kind !== "operator" || (t1.value !== "&" && t1.value !== "|")) {
        return sub0; // it's ok we consumed a valid expression, let the higher ups deal with the rest
    }
    nextToken(state);

    // let's force proper parenthesis everywhere, so I don't have to deal with associativity
    const sub1 = parseAtomicExpression(state);
    if (isError(sub1)) {
        return sub1;
    }

    return {
        kind: "exprOp",
        operator: t1.value,
        left: sub0,
        right: sub1,
        source: merge(sub0.source, sub1.source)
    };
}

function parseAtomicExpression(state: ParseState): AstResult {
    const t0 = peek(state);
    if (!t0) {
        return nextToken(state) as ParseError;
    } else if (t0.kind === "operator" && t0.value === "(") {
        return parseParenthesizedExpression(state);
    } else {
        return parseLinkOrFlagOp(state);
    }
}

function parseParenthesizedExpression(state: ParseState): AstResult {
    const open = nextToken(state); // consume (
    if (open.kind !== "operator" || open.value !== "(") {
        throw new Error("parseParenthesizedExpression called on invalid data");
    }

    const sub = parseExpression(state);

    let closed = nextToken(state); // consume )
    if (isError(closed)) {
        return closed;
    } else if (closed.kind !== "operator" || closed.value !== ")") {
        return makeError("parenthesized expression is not closed", merge(open.source, closed.source));
    }

    return sub;
}

function parseLinkOrFlagOp(state: ParseState): AstResult {
    const t0 = nextToken(state);
    if (isError(t0)) {
        return t0;
    } else if (t0.kind !== "string") {
        return makeError(`unexpected token ${JSON.stringify(t0)}`, t0.source);
    }

    const t1 = nextToken(state);
    if (isError(t1)) {
        return t1;
    } else if (t1.kind !== "operator" || (
            t1.value !== "=" && t1.value !== "!=" &&
            t1.value !== "~" && t1.value !== "!~" &&
            t1.value !== ">" && t1.value !== "!>" &&
            t1.value !== "<" && t1.value !== "!<" &&
            t1.value !== "+" && t1.value !== "-")) {
        return makeError(`unexpected token ${JSON.stringify(t1)}`, t1.source);
    }

    if (t1.value === "+" || t1.value === "-") {
        return {
            kind: "flagOp",
            operator: t1.value,
            property: t0.value,
            source: merge(t0.source, t1.source)
        };
    }

    const t2 = nextToken(state);
    if (isError(t2)) {
        return t2;
    } else if (t2.kind !== "string") {
        return makeError("unexpected token", t2.source);
    }

    return {
        kind: "linkOp",
        operator: t1.value,
        property: t0.value,
        target: t2.value,
        source: merge(t0.source, t2.source)
    };
}

function peek(state: ParseState): Token | undefined {
    return state.data[state.offset];
}

function nextToken(state: ParseState): Token | ParseError {
    const t = state.data[state.offset];
    if (!t) {
        let index = 0;
        if (state.offset > 0) {
            index = state.data[state.offset - 1]!.source.end;
        }

        return makeError("unexpected end of input", makeSpan(index, index));
    }
    ++state.offset;
    return t;
}

function buildFilter(ast: Expression): Filter {
    switch (ast.kind) {
        case "exprOp":
            return buildExpressionFilter(ast);
        case "flagOp":
            return buildFlagFilter(ast);
        case "linkOp":
            return buildLinkFilter(ast);
    }
}

function buildExpressionFilter(ast: Expression): Filter {
    if (ast.kind !== "exprOp") {
        throw new Error("buildExpressionFilter called with invalid data");
    }

    const l = buildFilter(ast.left);
    const r = buildFilter(ast.right);

    switch (ast.operator) {
        case "&":
            return intoFilter(function(e: mm.Entity): boolean {
                return l(e) && r(e);
            });
        case "|":
            return intoFilter(function(e: mm.Entity): boolean {
                return l(e) || r(e);
            });
    }
}

function buildFlagFilter(ast: Expression): Filter {
    if (ast.kind !== "flagOp") {
        throw new Error("buildFlagFilter called with invalid data");
    }

    const property = ast.property;
    switch (ast.operator) {
        case "+":
            return intoFilter(function(e: mm.Entity): boolean {
                return filterFlag(e, property);
            });
        case "-":
            return intoFilter(function(e: mm.Entity): boolean {
                return !filterFlag(e, property);
            });
    }
}

function buildLinkFilter(ast: Expression): Filter {
    if (ast.kind !== "linkOp") {
        throw new Error("buildLinkFilter called with invalid data");
    }

    const property = ast.property;
    const target = ast.target;
    switch (ast.operator) {
        case "=":
            return intoFilter(function(e: mm.Entity): boolean {
                return filterValueEquals(e, property, target, false);
            });
        case "!=":
            return intoFilter(function(e: mm.Entity): boolean {
                return !filterValueEquals(e, property, target, false);
            });
        case "~":
            return intoFilter(function(e: mm.Entity): boolean {
                return filterValuesSimilar(e, property, target);
            });
        case "!~":
            return intoFilter(function(e: mm.Entity): boolean {
                return !filterValuesSimilar(e, property, target);
            });
        case ">":
            return intoFilter(function(e: mm.Entity): boolean {
                return filterValueEquals(e, property, target, true);
            });
        case "!>":
            return intoFilter(function(e: mm.Entity): boolean {
                return !filterValueEquals(e, property, target, true);
            });
        case "<":
            return intoFilter(function(e: mm.Entity): boolean {
                return filterBackLinkEquals(e, property, target);
            });
        case "!<":
            return intoFilter(function(e: mm.Entity): boolean {
                return !filterBackLinkEquals(e, property, target);
            });
    }
}

function intoFilter(f: (e: mm.Entity) => boolean): Filter {
    (f as any).kind = "filter";
    return f as Filter;
}

function filterFlag(e: mm.Entity, property: string): boolean {
    if (property === "id") { // all entities have an id
        return true;
    }

    const p = e.properties.get(property);
    if (p) {
        return true;
    } else {
        return false;
    }
}

function filterValuesSimilar(e: mm.Entity, property: string, target: string): boolean {
    if (property === "id") {
        return e.id.includes(target);
    }

    const vs = e.properties.get(property);
    if (!vs) {
        return false;
    } else if (vs.length === 0) {
        return false;
    } else {
        for (const l of vs) {
            if (l.text.includes(target)) {
                return true;
            }
        }
        return false;
    }
}

function filterValueEquals(e: mm.Entity, property: string, target: string, mustLink: boolean): boolean {
    if (property === "id") {
        return e.id === target && !mustLink;
    }

    const vs = e.properties.get(property);
    if (!vs) {
        return false;
    } else if (vs.length === 0) {
        return false;
    } else {
        for (const l of vs) {
            if (l.text === target) {
                return !mustLink || l.kind === "link";
            }
        }
        return false;
    }
}

function filterBackLinkEquals(e: mm.Entity, property: string, target: string): boolean {
    const p = e.backLinks.get(property);
    if (!p) {
        return false;
    } else {
        for (const l of p) {
            if (l.text === target) {
                return true;
            }
        }
        return false;
    }
}

import * as mm from "./mindmap.js"

export type Navigator = (target: string) => void;
export type CustomRenderer = (p: mm.ValueList, e: mm.Entity, n: Navigator) => HTMLElement | null;
export interface CustomRendererDictionary {
    [p: string]: CustomRenderer;
}
export interface PropertySortDefinition {
    [p: number]: string | string[];
    ignore?: string | string[];
}

export interface ExtensionDefinition {
    propertySorting?: PropertySortDefinition;
    hideInSidebar?: string[];
    customRenderers?: CustomRendererDictionary;
}


export interface Extension {
    propertyToPriority: (p: string) => number | "ignore";
    customRenderers: CustomRendererDictionary;

    hideInSidebar: { [property: string]: boolean };
}

export function buildExtension(def?: ExtensionDefinition): Extension {
    if (!def) {
        return { propertyToPriority: () => 0, customRenderers: {}, hideInSidebar: {} };
    }

    const map: { [p: string]: number | "ignore" } = {};
    const mapFn = (p: string) => map[p] ?? 0;

    if (def.propertySorting) {
        for (const priorityKey of Object.keys(def.propertySorting)) {
            if (priorityKey === "ignore") { continue; }

            const priority = Number(priorityKey); // this should be safe, see the definition of propertySorting

            const sortDef = def.propertySorting[priority]; // not using Object.entries to preserve type info

            if (typeof sortDef === "string") {
                map[sortDef] = priority;
            } else if (sortDef instanceof Array) {
                for (const p of sortDef) {
                    map[p] = priority;
                }
            } else {
                throw new Error(`'${sortDef}' in ExtensionDefinition.propertySorting[${priority}] is not a string or string[]`);
            }
        }

        const ignores = def.propertySorting.ignore;
        if (ignores === undefined) {
            // do nothing
        } else if (typeof ignores === "string") {
            map[ignores] = "ignore";
        } else if (ignores instanceof Array) {
            for (const p of ignores) {
                map[p] = "ignore";
            }
        } else {
            throw new Error(`'${ignores}' in ExtensionDefinition.propertySorting.ignore is not a string or string[]`);
        }
    }

    const hideInSidebar: { [property: string]: boolean } = {};
    if (def.hideInSidebar) {
        def.hideInSidebar.forEach((property: string) => {
            hideInSidebar[property] = true;
        });
    }

    const renderers = def.customRenderers ?? {};

    return { propertyToPriority: mapFn, customRenderers: renderers, hideInSidebar: hideInSidebar };
}

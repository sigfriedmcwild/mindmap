//------------------------------------------------------------------------------
// Raw types
//------------------------------------------------------------------------------

export type Data = EntityList | readonly EntityList[];
export type EntityList = readonly Entity[];

export interface Entity {
    readonly id: string;
    readonly [property: string]: PropertyValue;
}

export type PropertyValue = PropertyValueFlag | PropertyValueSingle | PropertyValueMultiple | PropertyValueTagged;
export type PropertyValueFlag = true;
export type PropertyValueSingle = string;
export type PropertyValueMultiple = readonly string[];
export type PropertyValueTagged = { readonly [v: string]: TagValue };

export type TagValue = TagValueNone | TagValueSingle | TagValueMultiple;
export type TagValueNone = true;
export type TagValueSingle = string;
export type TagValueMultiple = readonly string[];

//------------------------------------------------------------------------------
// Type assertions

export function isRawPropertyValue(p?: PropertyValue): p is PropertyValue {
    if (p === undefined || p === null) {
        return false;
    } else if (typeof p === "boolean") {
        return true;
    } else if (typeof p === "string") {
        return true;
    } else if (p instanceof Array) {
        for (const pv of p) {
            if (typeof pv !== "string") {
                return false;
            }
        }
    }

    return true;
}

export function isRawEntity(e?: Entity): e is Entity {
    if (e === undefined || e === null) {
        return false;
    }

    for (const k of Object.keys(e)) {
        if (!isRawPropertyValue(e[k])) {
            return false;
        }
    }

    if (typeof e.id !== "string") {
        return false;
    }

    return true;
}

export function isRawData(d?: Data): d is Data {
    if (d === undefined || d === null) {
        return false;
    } else if (!(d instanceof Array)) {
        return false;
    }

    for (const rawEntityOrSublist of d) {
        if (rawEntityOrSublist instanceof Array) {
            for (const rawEntity of rawEntityOrSublist) {
                if (!isRawEntity(rawEntity)) {
                    return false;
                }
            }
        } else if (!isRawEntity(rawEntityOrSublist)) {
            return false;
        }
    }

    return true;
}

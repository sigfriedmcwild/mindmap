import * as mm_raw from "../engine/raw_data";

// Short hands and other things

// my_type:
//  reflexive (r): requires no action to activate, may create an action
//  simple (s): requires an action to activate
//  supplemental (+): enhances an action or applying a static value, never creates an action
//
// note that supplemental covers more than raw, and reflexive less. For example
// excellencies are now always supplemental as are perfect defences

// my_keywords:
//  attack(dual|uniform|decisive|withering|gambit|special, unblockable|undodgeable, aoe|multi): creates or enhances an attack of the specified type, ignores the specified defence, hits multiple targets in an area or produces multiple attacks
//  aura: see standard keyword
//  clash: see standard keyword
//  defence(any|decisive|withering, evasion|parry|soak|hardness|uncountable): can be used when defending against an attack of the specified type, can reduce uncountable damage
//  element(air|earth|fire|water|wood, aura|balanced): see dragon blood elemental keywords, multi element charms are indicated as "E1&E2" (inclusive) or "E1|E2" (exclusive)
//  excellency: see standard keyword
//  form: see standard keyword
//  join battle: enhances a join battle roll
//  movement: creates or enhances movement actions
//  mute: see standard keyword
//  non combat: creates or enhances non combat actions
//  perilous: see standard keyword
//  reset: this charm has a reset condition
//  stackable: see standard keyword
//  stealth: creates or enhances stealth actions or checks

export const raw: mm_raw.Data = [
    { //------------------------------------------------------------------------
        id: "Brawl",
        kind: "ability"
    }, //-----------------------------------------------------------------------
    {
        id: "Become the Hammer",
        kind: "charm",
        exaltation: "Dragon Blooded",
        ability: "Brawl", // attribute: "...", // for attribute charms, instead of ability
        cost: {motes: "X"},
        requirements: {ability: "1", essence: "1"},
        type: ["Supplemental", "Reflexive"],
        keywords: ["Balanced", "Excellency", "Uniform", "Water"],
        duration: "Instant",
        my_keywords: {
            "attack": "uniform",
            "defence": "any",
            "element": ["water", "balanced"],
            "excellency": true,
            "non combat": true,
        },
        my_type: "supplemental",
    },
    { //------------------------------------------------------------------------
        id: "Air Dragon Style",
        kind: {"ability": "martial art"},
        weapons: ["unarmed", "chakram"],
        armor: "light",
        source: "5e WFHW preview 379",
    }, //-----------------------------------------------------------------------
    {
        id: "Air Dragon's Sight",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "1", initiative: "1"},
        requirements: {ability: "2", essence: "1"},
        type: "Reflexive",
        keywords: ["Air", "Perilous"],
        duration: "Instant",
        my_keywords: {
            "defence": ["any", "evasion"],
            "element": "air",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 379",
    },
    {
        id: "Cloud-Treading Method",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "5"},
        requirements: {ability: "3", essence: "1"},
        type: "Supplemental",
        keywords: ["Air", "Mute"],
        duration: "Instant",
        my_keywords: {
            "element": "air",
            "movement": true,
            "mute": true,
            "stealth": true,
        },
        my_type: "supplemental",
        source: "5e WFHW preview 379",
    },
    {
        id: "Wind Dragon Speed",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "5"},
        requirements: {ability: "3", essence: "2", "Cloud-Treading Method": true},
        type: "Reflexive",
        keywords: ["Air"],
        duration: "Instant",
        my_keywords: {
            "element": "air",
            "movement": true,
        },
        my_type: "reflexive",
        source: "5e WFHW preview 380",
    },
    {
        id: "Breath-Seizing Technique",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "3", initiative: "2", willpower: "1"},
        requirements: {ability: "4", essence: "2", "Air Dragon's Sight": true},
        type: "Simple",
        keywords: ["Air", "Decisive-Only", "Stackable"],
        duration: "Instant",
        my_keywords: {
            "attack": "decisive",
            "element": "air",
            "stackable": true,
        },
        my_type: "simple",
        source: "5e WFHW preview 380",
    },
    {
        id: "Air Dragon Form",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "8"},
        requirements: {ability: "4", essence: "2", "Breath-Seizing Technique": true, "Wind Dragon Speed": true},
        type: "Simple",
        keywords: ["Air", "Form", "Mute"],
        duration: "Scene",
        my_keywords: {
            "attack": "withering",
            "defence": ["any", "evasion"],
            "element": "air",
            "form": true,
            "movement": true,
            "mute": true,
            "stealth": true,
        },
        my_type: "simple",
        source: "5e WFHW preview 381",
    },
    {
        id: "Shroud of Unseen Winds",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "4"},
        requirements: {ability: "4", essence: "2", "Air Dragon Form": true},
        type: "Supplemental",
        keywords: ["Air", "Mute", "Perilous"],
        duration: "Instant",
        my_keywords: {
            "element": "air",
            "stealth": true,
        },
        my_type: "simple",
        source: "5e WFHW preview 381",
    },
    {
        id: "Avenging Wind Strike",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "3", willpower: "1"},
        requirements: {ability: "4", essence: "3", "Air Dragon Form": true},
        type: "Supplemental",
        keywords: ["Air", "Decisive-Only"],
        duration: "Instant",
        my_keywords: {
            "attack": "decisive",
            "element": "air",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 381",
    },
    {
        id: "Lightning Strike Style",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "8", willpower: "1"},
        requirements: {ability: "5", essence: "3", "Avenging Wind Strike": true},
        type: "Supplemental",
        keywords: ["Air", "Decisive-Only"],
        duration: "Instant",
        my_keywords: {
            "attack": "decisive",
            "element": "air",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 382",
    },
    {
        id: "Thunderclap Kata",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "7", initiative: "5"},
        requirements: {ability: "5", essence: "3", "Avenging Wind Strike": true},
        type: "Simple",
        keywords: ["Air", "Perilous"],
        duration: "Instant",
        my_keywords: {
            "attack": ["special", "aoe"],
            "element": "air",
            "stealth": true,
        },
        my_type: "simple",
        source: "5e WFHW preview 382",
    },
    {
        id: "Tornado Offense Technique",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "10", willpower: "1"},
        requirements: {ability: "5", essence: "4", "Lightning Strike Style": true},
        type: "Simple",
        keywords: ["Air", "Decisive-Only"],
        duration: "Instant",
        my_keywords: {
            "attack": ["decisive", "aoe"],
            "element": "air",
        },
        my_type: "simple",
        source: "5e WFHW preview 382",
    },
    {
        id: "Wrathful Winds Kiai",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "15", willpower: "1"},
        requirements: {ability: "5", essence: "4", "Thunderclap Kata": true},
        type: "Simple",
        keywords: ["Air", "Withering-Only"],
        duration: "Instant",
        my_keywords: {
            "attack": ["special", "aoe", "unblockable"],
            "element": "air",
            "reset": true,
        },
        my_type: "simple",
        source: "5e WFHW preview 380",
    },
    {
        id: "Hurricane Combat Method",
        kind: "charm",
        ability: "Air Dragon Style",
        cost: {motes: "6", willpower: "1", extra: true},
        requirements: {ability: "5", essence: "5", "Shroud of Unseen Winds": true, "Tornado Offense Technique": true, "Wrathful Winds Kiai": true},
        type: "Permanent",
        keywords: ["Air"],
        duration: "Permanent",
        my_keywords: {
            "attack": "uniform",
            "defence": ["any", "evasion"],
            "element": "air",
            "stealth": true,
        },
        my_type: "simple",
        source: "5e WFHW preview 380",
    },
    { //------------------------------------------------------------------------
        id: "Earth Dragon Style",
        kind: {"ability": "martial art"},
        weapons: ["unarmed", "tetsubo"],
        armor: ["light", "medium", "heavy"],
    }, //-----------------------------------------------------------------------
    {
        id: "Stone Dragon Skin",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "5", initiative: "1"},
        requirements: {ability: "2", essence: "1"},
        type: "Reflexive",
        keywords: ["Dual", "Earth", "Perilous"],
        duration: "Instant",
        my_keywords: {
            "defence": ["any", "parry", "soak", "hardness"],
            "element": "earth",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 384",
    },
    {
        id: "Force of the Mountain",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "3"},
        requirements: {ability: "3", essence: "1"},
        type: "Supplemental",
        keywords: ["Dual", "Earth"],
        duration: "Instant",
        my_keywords: {
            "attack": "dual",
            "element": "earth",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 384",
    },
    {
        id: "Force of the Mountain",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "3"},
        requirements: {ability: "2", essence: "1"},
        type: "Supplemental",
        keywords: ["Dual", "Earth"],
        duration: "Instant",
        my_keywords: {
            "attack": "dual",
            "element": "earth",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 384",
    },
    {
        id: "Stillness-of-Stone Atemi",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "2", initiative: "X"},
        requirements: {ability: "3", essence: "2", "Force of the Mountain": true},
        type: "Supplemental",
        keywords: ["Earth", "Withering-only"],
        duration: "Instant",
        my_keywords: {
            "attack": "withering",
            "element": "earth",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 385",
    },
    {
        id: "Unmoving Mountain Stance",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "6"},
        requirements: {ability: "4", essence: "2", "Stone Dragon Skin": true},
        type: "Reflexive",
        keywords: ["Dual", "Earth", "Perilous"],
        duration: "Instant",
        my_keywords: {
            "defence": "any",
            "element": "earth",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 385",
    },
    {
        id: "Earth Dragon Form",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "9"},
        requirements: {ability: "4", essence: "2", "Stillness-of-Stone Atemi": true, "Unmoving Mountain Stance": true},
        type: "Simple",
        keywords: ["Earth", "Form"],
        duration: "Scene",
        my_keywords: {
            "attack": "uniform",
            "defence": "soak",
            "element": "earth",
            "form": true,
        },
        my_type: "simple",
        source: "5e WFHW preview 385",
    },
    {
        id: "Ghost-Grounding Blow",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "5", initiative: "5", willpower: "1"},
        requirements: {ability: "4", essence: "2", "Earth Dragon Form": true},
        type: "Reflexive",
        keywords: ["Earth", "Perilous", "Withering-only"],
        duration: "Instant",
        my_keywords: {
            "attack": "withering",
            "element": "earth",
            "perilous": true,
        },
        my_type: "supplemental",
        source: "5e WFHW preview 386",
    },
    {
        id: "Ghost-Grounding Blow",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "5", initiative: "5", willpower: "1"},
        requirements: {ability: "4", essence: "2", "Earth Dragon Form": true},
        type: "Reflexive",
        keywords: ["Earth", "Perilous", "Withering-only"],
        duration: "Instant",
        my_keywords: {
            "attack": "withering",
            "element": "earth",
            "perilous": true,
        },
        my_type: "reflexive",
        source: "5e WFHW preview 386",
    },
    {
        id: "Earthshaker Attack",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "10", initiative: "5", willpower: "1"},
        requirements: {ability: "5", essence: "3", "Earth Dragon Form": true},
        type: "Simple",
        keywords: ["Decisive-only", "Earth"],
        duration: "Instant",
        my_keywords: {
            "attack": ["decisive", "aoe", "special"],
            "element": "earth",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 386",
    },
    {
        id: "Shattering Fist Strike",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "3", willpower: "1"},
        requirements: {ability: "5", essence: "3", "Earth Dragon Form": true},
        type: "Supplemental",
        keywords: ["Decisive-only", "Earth"],
        duration: "Instant",
        my_keywords: {
            "attack": "gambit",
            "element": "earth",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 387",
    },
    {
        id: "Weapon-Breaking Defense Technique",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "10", willpower: "1"},
        requirements: {ability: "5", essence: "3", "Shattering Fist Strike": true},
        type: "Reflexive",
        keywords: ["Clash", "Decisive-only", "Earth"],
        duration: "Instant",
        my_keywords: {
            "attack": "gambit",
            "clash": true,
            "element": "earth",
        },
        my_type: "reflexive",
        source: "5e WFHW preview 387",
    },
    {
        id: "Avalanche Method",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "20", willpower: "1"},
        requirements: {ability: "5", essence: "5", "Weapon-Breaking Defense Technique": true},
        type: "Simple",
        keywords: ["Decisive-only", "Earth"],
        duration: "Instant",
        my_keywords: {
            "attack": ["decisive", "multi"],
            "element": "earth",
            "perilous": true,
        },
        my_type: "simple",
        source: "5e WFHW preview 388",
    },
    {
        id: "Hungry Earth Strike",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "10", initiative: "6", willpower: "1"},
        requirements: {ability: "5", essence: "4", "Earthshaker Attack": true, "Ghost-Grounding Blow": true},
        type: "Supplemental",
        keywords: ["Earth", "Perilous", "Withering-only"],
        duration: "Instant",
        my_keywords: {
            "attack": "withering",
            "element": "earth",
            "perilous": true,
        },
        my_type: "supplemental",
        source: "5e WFHW preview 388",
    },
    {
        id: "Perfection of Earth Body",
        kind: "charm",
        ability: "Earth Dragon Style",
        cost: {motes: "6", initiative: "6", willpower: "1", extra: true},
        requirements: {ability: "5", essence: "5", "Avalanche Method": true, "Hungry Earth Strike": true},
        type: "Permanent",
        keywords: ["Earth", "Perilous"],
        duration: "Permanent",
        my_keywords: {
            "attack": "dual",
            "defence": "soak",
            "element": "earth",
            "perilous": true,
        },
        my_type: "permanent",
        source: "5e WFHW preview 389",
    },
    { //------------------------------------------------------------------------
        id: "Fire Dragon Style",
        kind: {"ability": "martial art"},
        weapons: ["unarmed", "short sword"],
        armor: ["light", "medium"],
    }, //-----------------------------------------------------------------------
    {
        id: "Flash-Fire Technique",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "5", willpower: "1"},
        requirements: {ability: "3", essence: "1"},
        type: "Supplemental",
        keywords: ["Decisive-Only", "Fire"],
        duration: "Instant",
        my_keywords: {
            "attack": "decisive",
            "element": "fire",
            "join battle": true,
        },
        my_type: "supplemental",
        source: "5e WFHW preview 389",
    },
    {
        id: "Searing Edge Attack",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "3"},
        requirements: {ability: "3", essence: "1"},
        type: "Supplemental",
        keywords: ["Fire", "Withering-Only"],
        duration: "Instant",
        my_keywords: {
            "attack": "withering",
            "element": "fire",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 390",
    },
    {
        id: "Flame-Flicker Stance",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "3"},
        requirements: {ability: "3", essence: "2"},
        type: "Reflexive",
        keywords: ["Fire", "Perilous"],
        duration: "Instant",
        my_keywords: {
            "defence": ["any", "parry"],
            "element": "fire",
            "perilous": true,
        },
        my_type: "supplemental",
        source: "5e WFHW preview 390",
    },
    {
        id: "Perfect Blazing Blow",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "3", willpower: "1"},
        requirements: {ability: "4", essence: "2", "Searing Edge Attack": true},
        type: "Supplemental",
        keywords: ["Decisive-Only", "Fire"],
        duration: "Instant",
        my_keywords: {
            "attack": "decisive",
            "element": "fire",
            "perilous": true,
        },
        my_type: "supplemental",
        source: "5e WFHW preview 390",
    },
    {
        id: "Fire Dragon Form",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "7"},
        requirements: {ability: "4", essence: "2", "Flame-Flicker Stance": true, "Perfect Blazing Blow": true},
        type: "Simple",
        keywords: ["Fire", "Form"],
        duration: "Scene",
        my_keywords: {
            "attack": "uniform",
            "defence": ["decisive", "parry"],
            "element": "fire",
            "form": "true",
        },
        my_type: "simple",
        source: "5e WFHW preview 391",
    },
    {
        id: "God-Immolating Strike",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "5"},
        requirements: {ability: "4", essence: "2", "Fire Dragon Form": true},
        type: "Supplemental",
        keywords: ["Decisive-Only", "Fire"],
        duration: "Instant",
        my_keywords: {
            "attack": "decisive",
            "element": "fire",
        },
        my_type: "supplemental",
        source: "5e WFHW preview 391",
    },
    {
        id: "Essence-Igniting Nerve Strike",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "4", willpower: "1"},
        requirements: {ability: "4", essence: "2", "God-Immolating Strike": true},
        type: "Simple",
        keywords: ["Fire", "Withering-Only"],
        duration: "Instant",
        my_keywords: {
            "attack": "withering",
            "element": "fire",
        },
        my_type: "simple",
        source: "5e WFHW preview 391",
    },
    {
        id: "Essence-Igniting Nerve Strike",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "4", willpower: "1"},
        requirements: {ability: "4", essence: "2", "God-Immolating Strike": true},
        type: "Simple",
        keywords: ["Fire", "Withering-Only"],
        duration: "Instant",
        my_keywords: {
            "attack": "withering",
            "element": "fire",
        },
        my_type: "simple",
        source: "5e WFHW preview 391",
    },
    {
        id: "Overwhelming Fire Majesty Stance",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "6", initiative: "3"},
        requirements: {ability: "4", essence: "3", "Fire Dragon Form": true},
        type: "Reflexive",
        keywords: ["Fire", "Perilous"],
        duration: "Turn",
        my_keywords: {
            "defence": "any",
            "element": "fire",
            "perilous": true,
        },
        my_type: "reflexive",
        source: "5e WFHW preview 392",
    },
    {
        id: "Fiery Blade Attack",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "5", initiative: "1"},
        requirements: {ability: "5", essence: "3", "Fire Dragon Form": true},
        type: "Simple",
        keywords: ["Decisive-Only", "Fire"],
        duration: "Instant",
        my_keywords: {
            "attack": "decisive",
            "element": "fire",
        },
        my_type: "simple",
        source: "5e WFHW preview 392",
    },
    {
        id: "Breath of the Fire Dragon",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "15", willpower: "1"},
        requirements: {ability: "5", essence: "4", "Overwhelming Fire Majesty Stance": true},
        type: "Simple",
        keywords: ["Decisive-Only", "Fire"],
        duration: "Instant",
        my_keywords: {
            "attack": ["decisive", "unblockable", "aoe"],
            "element": "fire",
            "reset": true,
        },
        my_type: "simple",
        source: "5e WFHW preview 392",
    },
    {
        id: "Smoldering Wound Attack",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "5", willpower: "1"},
        requirements: {ability: "5", essence: "4", "Essence-Igniting Nerve Strike": true, "Fiery Blade Attack": true},
        type: "Reflexive",
        keywords: ["Decisive-Only", "Fire"],
        duration: "Instant",
        my_keywords: {
            "attack": "decisive",
            "element": "fire",
        },
        my_type: "reflexive",
        source: "5e WFHW preview 393",
    },
    {
        id: "Consuming Might of the Fire Dragon",
        kind: "charm",
        ability: "Fire Dragon Style",
        cost: {motes: "5", initiative: "1", extra: true},
        requirements: {ability: "5", essence: "5", "Breath of the Fire Dragon": true, "Smoldering Wound Attack": true},
        type: "Permanent",
        keywords: ["Dual", "Fire"],
        duration: "Instant",
        my_keywords: {
            "attack": "dual",
            "defence": "any",
            "element": "fire",
        },
        my_type: "simple",
        source: "5e WFHW preview 393",
    },
]

import * as du from "../ui/dom_utils.js";
import * as mm from "../engine/mindmap.js";
import * as mm_ext from "../engine/extensions.js";

export const extension: mm_ext.ExtensionDefinition = {
    propertySorting: {
        "-11": "kind",
        "-10": ["ability", "attribute"],
        "-9": "cost",
        "-8": "type",
        "-7": "duration",
        "-6": "keywords",
        "-5": "my_type",
        "-4": "my_keywords",
        ignore: [
            "requirements",
        ],
    },
    customRenderers: {
        ability: (p, e, n) => renderAbilityAndRequirements(p, e, n),
        cost: (p, e, n) => renderCost(p),
    },
};

function renderAbilityAndRequirements(_0: mm.ValueList, e: mm.Entity, n: mm_ext.Navigator): HTMLElement | null {
    const [kind, _1] = getValue(e, "kind"); // TODO support multiple kinds

    if (kind !== "charm") {
        return null;
    }

    const text: HTMLElement[] = [];

    let trait: string
    let traitId: string | undefined;
    let traitType: "ability" | "attribute";
    {
        const [ability, abilityId] = getValue(e, "ability", true);
        const [attribute, attributeId] = getValue(e, "attribute", true);
        if (!ability && !attribute) {
            throw new Error(`renderAbilityAndRequirements called on '${kind}' entity with no associated ability or attribute`);
        } else if (ability && attribute) {
            throw new Error(`renderAbilityAndRequirements called on '${kind}' entity with associated ability and attribute`);
        }
        [trait, traitId, traitType] = ability ?
            [ability!, abilityId, "ability"] :
            [attribute!, attributeId, "attribute"];
    }

    const reqs = e.properties.get("requirements");

    if (!reqs) {
        throw new Error(`renderAbilityAndRequirements called on '${kind}' entity with missing 'requirements' property`);
    }

    let traitMin = 0;
    let essenceMin = 0;
    let prereqs: [string, string?][] = []; // charm name, charm id
    for (const l of reqs) {
        if (l.text === traitType) {
            traitMin = Number(l.tags[0]);
        } else if (l.text === "essence") {
            essenceMin = Number(l.tags[0]);
        } else {
            prereqs.push([l.text, l.kind === "link" ? l.target.id : undefined]);
        }
    }

    text.push(du.text(`Mins: `));
    addLinkText(text, n, trait, traitId);
    text.push(du.text(`: ${traitMin}, Essence: ${essenceMin}`));
    text.push(du.newLine());
    text.push(du.text(`Prerequisites: `));
    for (const p of prereqs) {
        addLinkText(text, n, p[0], p[1]);
        text.push(du.text(", "));
    }
    text.pop(); // drop trailing ", "

    return du.div(text);
}

function renderCost(p: mm.ValueList): HTMLElement | null {
    if (p.length === 0) {
        return null;
    }

    const text: HTMLElement[] = [];

    let extra = false;
    const cost: { [k: string]: string } = {};
    for (const l of p) {
        if (l.text === "extra") {
            extra = true;
            continue;
        }

        if (l.tags[0]) {
            cost[l.text] = l.tags[0];
            continue;
        }

        throw new Error(`found cost '${l.text}' with no associated tag`);
    }

    text.push(du.text("Cost: "));

    if (extra) {
        text.push(du.text("- (+"));
    }

    let first = true;
    let format = (value: string | undefined, tag: string) => {
        if (value) {
            text.push(du.text(`${first ? "" : ", "}${value}${tag}`));
            first = false;
        }
    };

    format(cost.motes, "m");
    format(cost.initiative, "i");
    format(cost.willpower, "wp");
    format(cost.anima, "a");
    format(cost.bashingHealthLevels, "hl");
    format(cost.lethalHealthLevels, "lhl");
    format(cost.aggravatedHealthLevels, "ahl");
    format(cost.experience, "xp");
    format(cost.silverCraftPoints, "sxp");
    format(cost.goldCraftPoints, "gxp");
    format(cost.whiteCraftPoints, "wxp");
    format(cost.salientCraftPoints, "s/g/wxp");

    if (text.length === 1 + (extra ? 1 : 0)) {
        if (extra) {
            throw new Error(`found cost marked as additional cost, but with no actual values`);
        }
        text.push(du.text("-"));
    }

    if (extra) {
        text.push(du.text(")"));
    }

    return du.div(text);
}

function getValue(e: mm.Entity, n:string): [string, string?];
function getValue(e: mm.Entity, n:string, opt: false): [string, string?];
function getValue(e: mm.Entity, n:string, opt: true): [string?, string?];
function getValue(e: mm.Entity, n: string, opt?: boolean): [string?, string?] {
    let p = e.properties.get(n);
    if (!p) {
        if (opt) {
            return [undefined, undefined];
        } else {
            return ["?", undefined];
        }
    } else if (p.length === 0) {
        throw new Error(`renderStatline called on entity with no values for '${n}'`);
    } else if (!p[0]) {
        throw new Error(`renderStatline called on entity with with invalid first value for '${n}`);
    }

    return [
        p[0].text,
        p[0].kind === "link" ? p[0].target.id : undefined
    ];
}

function addLinkText(
    elements: HTMLElement[],
    n: mm_ext.Navigator,
    v?: string,
    vt?: string
): void {
    if (!v) {
        return;
    }

    if (vt) {
        elements.push(du.link(v, () => n(vt!)));
    } else {
        elements.push(du.text(v));
    }
}

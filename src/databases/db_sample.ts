import * as mm_raw from "../engine/raw_data";

export const raw: mm_raw.Data = [
    [
        {
            id: "Koyomi",
            kind: { "person": true, "murderhobo": "oh boy", "failure": ["diplomacy", "restraint"], "Solar": true },
            exaltation: "Solar",
            caste: "Night",
            pc: true,
            ties: {
                "Lilly": ["respect", "major"],
                "Rook": ["save him from himself", "minor"],
            },
            principles: {
                "I will pee in Lord Malice's eyesocket": "major",
                "I am the chosen bearer of the weapons of endings": true,
            }
        },
        {
            id: "Lilly",
            kind: ["person", "diplomat"],
            exaltation: "Solar",
            caste: "Eclipse",
            pc: true,
            ties: ["Koyomi", "Rook", "Vanessa"],
        },
    ],
    [
        {
            id: "Rook",
            kind: "person",
            exaltation: "Solar",
            caste: "Twilight",
            pc: true,
            ties: "Lilly",
        },
        {
            id: "Vanessa",
            kind: "person",
            exaltation: "Lunar",
            caste: "Full Moon",
        },
        {
            id: "Solar",
            kind: "exaltation",
        },
        {
            id: "Lunar",
            kind: "exaltation",
        },
        {
            id: "Night",
            kind: "caste",
        },
        {
            id: "Twilight",
            kind: "caste",
        },
        {
            id: "Full Moon",
            kind: "caste",
        }
    ],
    [
        {
            id: "test",
            f0: true,
            f1: "",
            f2: [],
            f3: {},
            ps0: "string",
            ps1: ["string in array"],
            ps2: { "string in object": true, },
            pm0: ["one", "two"],
            pm1: {"one": true, "two": true, },
            pt0: {"tagless": true, "taggee": "tagger", "taggiest": ["tag", "tog",],},
            l0: "test",
            l1: ["test"],
            l2: {"test": true,},
            l3: {"test": "ping",},
            nl0: {"toast": "test"},
        }
    ],
];

import * as mm from "../engine/mindmap.js";
import * as mm_ext from "../engine/extensions.js";

export const extension: mm_ext.ExtensionDefinition = {
    propertySorting: {
        "-1": "kind",
        "2": "pc",
    },
    hideInSidebar: [

    ],
    customRenderers: {

    },
};

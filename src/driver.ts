import * as db_exalted from "./databases/db_exalted.js";
import * as db_l5r from "./databases/db_l5r.js";
import * as db_sample from "./databases/db_sample.js";
import * as mm from "./engine/mindmap.js";
import * as mm_ext from "./engine/extensions.js";
import * as mm_raw from "./engine/raw_data.js";
import * as src from "./engine/search.js";
import * as du from "./ui/dom_utils.js";
import * as state from "./state.js";
import * as ui from "./ui/ui.js";

//------------------------------------------------------------------------------
// TODO
// 1. document based "schema"
//      provide list of priority -> property mappings for sorting in the detail view
//      run by the controller?
// 2. detail view render plugin
//      provide property to render function mapping (combine with 1 to control sorting)
//      run by view? or in the controller and passed in as a prerendered child to the view?
//      exported as thunk to keep impl clean
// 4. use kind for optional filtering of suggestions?
// 5. SERIALIZATION!!!
// 6. friendly id?
// 7. deeplinks
//------------------------------------------------------------------------------

function dumpCookedDataset(data: mm.Data, element: HTMLElement): void {
    du.removeChildren(element);

    for (const [_, e] of data.byId) {
        const block = element.appendChild(du.div());
        block.appendChild(du.text(`id: ${e.id}`));
        block.appendChild(du.newLine());

        for (const [p, vs] of e.properties) {
            if (vs.length === 0) {
                block.appendChild(du.text(`${p}: true`));
            } else {
                const m = vs.length > 1;

                let txt = `${p}: `;
                if (m) { txt += '[ '; }
                for (let v of vs) {
                    txt += `${v.text}${v.kind === "link" ? '*' : ''}`;

                    if (v.tags.length > 0) {
                        txt += "( ";
                        for (const t of v.tags) {
                            txt += `${t}, `;
                        }
                        txt += ")";
                    }

                    if (m) { txt += ', '; }
                }
                if (m) { txt += `]`; }

                block.appendChild(du.text(txt));
            }

            block.appendChild(du.newLine());
        }

        element.appendChild(du.horizontalRule());
    }
}

interface NavItem
{

}

class Navman {
    public constructor(data: mm.Data, extension: mm_ext.Extension, state: state.State) {
        this.data = data;
        this.extension = extension;
        this.state = state;
        this.backStack = [];
        this.forwardStack = [];
    }
    public go(i: state.Item): void {
        if (this.current) {
            this.backStack.push(this.current);
        }
        this.forwardStack = [];

        this.show(i);
    }
    public back(): void {
        const e = this.backStack.pop();
        if (!e) {
            return;
        }

        if (this.current) {
            this.forwardStack.push(this.current);
        }

        this.show(e);
    }
    public forward(): void {
        const e = this.forwardStack.pop();
        if (!e) {
            return;
        }

        if (this.current) {
            this.backStack.push(this.current);
        }

        this.show(e);
    }

    public onSearch(search: string): void {
        const i = state.makeItemFromQuery(search);
        this.go(i);
    }

    public onTabSelected(tab: ui.Tab, pinned: boolean): void {
        if (!tab.item) {
            dumpCookedDataset(this.data, g_mainPane);
        } else {
            showItem(tab.item as state.Item, pinned, this.data, this.extension, g_mainPane, this);
        }
    }

    private show(i: state.Item): void {
        this.state.onNavigate(i);

        this.current = i;
        if (this.tabBar) {
            this.tabBar.updateMainTab(ui.makeTab(i, i.display));
            this.tabBar.selectMainTab();
        }

        showItem(i, false, this.data, this.extension, g_mainPane, this);
    }

    public readonly data: mm.Data;
    public readonly extension: mm_ext.Extension;
    public readonly state: state.State;

    public tabBar?: ui.TabBar;
    private readonly backStack: state.Item[];
    private forwardStack: state.Item[];
    private current?: state.Item;
}

function makeNavigator(parent: HTMLElement, navman: Navman, documents: string[], selected: number): ui.Navigator {
    const navigator = ui.makeNavigator(documents, selected, "navigator");

    navigator.onBack = () => {
        navman.back();
    };
    navigator.onForward = () => {
        navman.forward();
    };

    const n = navigator.render();
    parent.appendChild(n);

    return navigator;
}

function makeTabBar(parent: HTMLElement, navman: Navman, state: state.State): ui.TabBar {
    const tabBar = ui.makeTabBar(ui.makeTab(null, "Home"), "tabbar");

    tabBar.onTabSelected = (tab: ui.Tab, pinned: boolean) => {
        navman.onTabSelected(tab, pinned);
    };
    tabBar.onPin = (tab: ui.Tab, index: number) => {
        const e = tab.item as state.Item;
        state.onPin(e, index);
    };
    tabBar.onUnpin = (tab: ui.Tab, index: number) => {
        const e = tab.item as state.Item;
        state.onUnpin(e, index);
    }

    const t = tabBar.render();
    parent.appendChild(t);

    return tabBar;
}

function makeSidebar(data: mm.Data, parent: HTMLElement, navman: Navman): ui.NestedList {
    const sidebar = ui.makeNestedList();

    // list by id:
    const byId = ui.makeListItem(undefined, "By Id:", sidebar);
    byId.sortPriority = 1;
    const byIdItems = ui.makeNestedList(byId);
    for (const [id, _] of data.byId) {
        ui.makeListItem(state.makeItemFromId(id), id, byIdItems);
    }

    const byProperty = ui.makeListItem(undefined, "By Property:", sidebar);
    byProperty.sortPriority = 2;
    const byPropertyItems = ui.makeNestedList(byProperty);
    for (const [p, valuesByProperty] of data.byProperty) {
        if (navman.extension.hideInSidebar[p]) {
            continue;
        }

        const pi = ui.makeListItem(undefined, p, byPropertyItems);

        if (p == "kind") {
            pi.sortPriority = -1;
        }

        const piItems = ui.makeNestedList(pi);
        for (const [v, values] of valuesByProperty) {
            if (v === "true") {
                for (const v of values) {
                    ui.makeListItem(state.makeItemFromId(v.id), v.id, piItems);
                }
            } else {
                const vi = ui.makeListItem(undefined, v, piItems);

                const viItems = ui.makeNestedList(vi);
                for (const v of values) {
                    ui.makeListItem(state.makeItemFromId(v.id), v.id, viItems);
                }
            }
        }
    }

    ui.setListClickHandler(sidebar, (e: ui.ListEntry) => {
        const i = e.item as state.Item | undefined;
        if (i) {
            navman.go(i);
        } else {
            throw new Error(`Unexpected on click event for list entry: ${JSON.stringify(e)}`);
        }
    });

    parent.appendChild(
        ui.renderNestedList(sidebar, "sidebar", ["mainPanelCol1"])
    );

    return sidebar;
}

function showItem(
    item: state.Item,
    pinned: boolean,
    data: mm.Data,
    extension: mm_ext.Extension,
    parent: HTMLElement,
    navman: Navman

): void {
    if (item.parsedQuery.kind === "error") {
        showError(item.rawQuery, item.parsedQuery, parent, navman);
    } else {
        const es = src.search(data, item.parsedQuery);

        if (es.length === 1) {
            showEntity(item.rawQuery, pinned, es[0]!, extension, parent, navman);
        } else {
            showEntityList(item.rawQuery, pinned, es, parent, navman);
        }
    }
}

function showError(
    search: string,
    e: src.ParseError,
    parent: HTMLElement,
    navman: Navman
): void {
    du.removeChildren(parent);

    const sb = new ui.SearchBar(search, true, "main_search");
    sb.onSearch = (s: string) => navman.onSearch(s);
    parent.appendChild(sb.render());

    parent.appendChild(du.text(e.message));
    parent.appendChild(du.newLine());
    parent.appendChild(du.text(`at (${e.source.begin}, ${e.source.end})`));
}

let g_currentEntity: mm.Entity;
function showEntity(
    search: string,
    pinned: boolean,
    entity: mm.Entity,
    extension: mm_ext.Extension,
    parent: HTMLElement,
    navman: Navman
): void {
    if (g_currentEntity == entity) {
        return;
    }

    du.removeChildren(parent);

    const panel = pinned ?
        ui.makePinnedEntryDetails(entity.id, search, "main") :
        ui.makeMainEntryDetails(entity.id, search, "main");

    for (const [pid, vs] of entity.properties) {
        console.log(`Looking at ${pid}`);
        const property = ui.makeEntryProperty(pid);

        const priority = extension.propertyToPriority(pid);
        if (priority === "ignore") {
            console.log(`    ignoring`);
            continue;
        }
        property.sortPriority = priority;
        console.log(`    processing`);

        const navigator = (id: string) => navman.go(state.makeItemFromId(id));
        const cr = extension.customRenderers[pid];
        if (cr) {
            console.log(`    has custom renderer`);
            property.customRenderer = () => cr(vs, entity, navigator);
        }

        property.values = [];
        for (const v of vs) {
            const ev = makeEntityPropertyValue(navman, v);
            property.values.push(ev);
        }

        panel.addProperty(property);
    }

    for (const [bid, b] of entity.backLinks) {
        const link = ui.makeEntryProperty(bid);
        link.values = [];
        for (const v of b) {
            const ev = makeEntityPropertyValue(navman, v);
            link.values.push(ev);
        }

        panel.addBacklink(link);
    }

    panel.onPropertyChanged = (change: string, property: ui.Property) => {
        if (change === "added") {
            console.log(`Added property '${property.display}' on ${entity.id}`);

            mm.addFlag(entity, property.display, navman.data);
        } else if (change === "removed") {
            console.log(`Removed property '${property.display}' from ${entity.id}`);

            mm.removeProperty(entity, property.display, navman.data);
        } else if (change === "valueChanged") {
            console.log(`Changed property: '${property.display}' on ${entity.id}`);
        }
    };

    panel.searchBar.onSearch = (s: string) => navman.onSearch(s);

    parent.appendChild(panel.render());
}

function showEntityList(
    search: string,
    pinned: boolean,
    entities: mm.Entity[],
    parent: HTMLElement,
    navman: Navman
): void {
    du.removeChildren(parent);

    const panel = pinned ?
        ui.makePinnedEntryList(search, "main") :
        ui.makeMainEntryList(search, "main");
    for (const e of entities) {
        const es = ui.makeEntrySummary(e.id);
        es.goToTarget = () => navman.go(state.makeItemFromId(e.id));

        const kind = e.properties.get("kind");
        if (kind && kind.length > 0) {
            for (const v of kind) {
                const ev = makeEntityPropertyValue(navman, v);
                es.values.push(ev);
            }
        }

        panel.addEntry(es);
    }

    panel.searchBar.onSearch = (s: string) => navman.onSearch(s);

    parent.appendChild(panel.render());
}

function makeEntityPropertyValue(navman: Navman, l: mm.Value | mm.BackLink): ui.PropertyValue {
    const v = ui.makePropertyValue(l.text);

    if (l.kind === "link" || l.kind === "backlink") {
        const kind = l.target.properties.get("kind");
        if (kind && kind.length > 0) {
            for (const k of kind) {
                v.valueExtras.push(k.text);
            }
        }

        v.goToTarget = () => navman.go(state.makeItemFromId(l.text));
    }

    for (const t of l.tags) {
        v.tags.push(t);
    }

    return v;
}

//------------------------------------------------------------------------------
// Document management
//------------------------------------------------------------------------------

interface Document {
    rawData: mm_raw.Data;
    extension?: mm_ext.ExtensionDefinition;
}

function selectDocument(
    rawData: mm_raw.Data,
    extensionDefinition: mm_ext.ExtensionDefinition | undefined,
    selected: number,
    documents: string[],
    onDocumentSelected: (name: string) => void
): void {
    du.removeChildren(g_navigatorPane);
    du.removeChildren(g_tabBarPane);
    du.removeChildren(g_sidebarPane);
    du.removeChildren(g_mainPane);

    const data = mm.buildData(rawData);
    const ext = mm_ext.buildExtension(extensionDefinition);

    const navman = new Navman(data, ext, g_state);

    const navigator = makeNavigator(g_navigatorPane, navman, documents, selected);
    const tabBar = makeTabBar(g_tabBarPane, navman, g_state);
    const sidebar = makeSidebar(data, g_sidebarPane, navman);

    navman.tabBar = tabBar;

    navigator.onDocumentSelected = onDocumentSelected;

    const restoreTabs = g_state.Tabs();

    if (restoreTabs.length > 0) {
        navman.go(restoreTabs[0]!);
    } else {
        throw new Error("Unexpected empty tab list");
    }

    for (let i = 1; i < restoreTabs.length; ++i) {
        const item = restoreTabs[i]!;
        tabBar.addTab(ui.makeTab(item, item.display));
    }

    tabBar.render();
}

function switchDocument(name?: string): void {
    name = name ?? "Sample";

    const doc = g_documents[name];
    if (doc) {
        g_state.onDocumentSelected(name);

        let docs = Object.keys(g_documents);
        let index = docs.indexOf(name);

        selectDocument(doc.rawData, doc.extension, index, docs, switchDocument);
    } else {
        switchDocument(Object.keys(g_documents)[0]);
    }
}

const element = document.getElementById("content");
if (!element) {
    throw new Error("Cannot find root element in page");
}

const g_state = new state.State();

const g_window = element.appendChild(du.div(undefined, undefined, ["core", "window"]));
const g_navigatorPane = g_window.appendChild(du.div(undefined, undefined, ["navigator"]));
const g_tabBarPane = g_window.appendChild(du.div(undefined, undefined, ["tabbar"]));
const g_sidebarPane = g_window.appendChild(du.div(undefined, undefined, ["sidebar"]));
const g_mainPane = g_window.appendChild(du.div(undefined, undefined, ["main"]));

const g_documents: {[k: string]: Document} = {
    "Exalted": { rawData: db_exalted.raw, extension: db_exalted.extension },
    "L5R": { rawData: db_l5r.raw, extension: db_l5r.extension },
    "Sample": { rawData: db_sample.raw, extension: db_sample.extension },
    "Sample2": { rawData: db_sample.raw, extension: db_sample.extension },
}

switchDocument(g_state.Document());

import * as du from "./dom_utils.js";

function addCssBase(e: HTMLElement, cssBase: string, suffix: string): void {
    e.classList.add(`${cssBase}${suffix}`);
}

function removeCssBase(e: HTMLElement, cssBase: string, suffix: string): void {
    e.classList.remove(`${cssBase}${suffix}`);
}

interface Sortable {
    readonly display: string,
    readonly sortPriority: number,
}

function elementSorter(l: Sortable, r: Sortable): number {
    if (l.sortPriority == r.sortPriority) {
        return l.display.localeCompare(r.display);
    } else {
        return l.sortPriority - r.sortPriority;
    }
}

//------------------------------------------------------------------------------
// Control
//------------------------------------------------------------------------------
export interface Control {
    readonly render: () => HTMLElement,
}

//------------------------------------------------------------------------------
// Search Bar
//------------------------------------------------------------------------------
export class SearchBar implements Control {
    public render(): HTMLDivElement {
        if (this.lazy_element) {
            return this.lazy_element;
        }

        this.lazy_element = du.div();
        addCssBase(this.lazy_element, this.cssBase, "");

        const tb = this.lazy_element.appendChild(du.textBox(this.current));
        addCssBase(tb, this.cssBase, "_text");
        if (this.active) {
            tb.onkeyup = (ev: KeyboardEvent) => {
                this.current = tb.value;
                if (ev.key === "Enter") {
                    this.onDoIt();
                }
            };
        } else {
            tb.disabled = true;
        }

        const b = this.lazy_element.appendChild(du.button("Go", () => this.onDoIt()));
        addCssBase(b, this.cssBase, "_button");
        if (!this.active) {
            b.disabled = true;
        }

        return this.lazy_element;
    }

    public constructor(initial: string, active: boolean, cssBase: string) {
        this.current = initial;
        this.active = active;
        this.cssBase = cssBase;
    }

    public search(): string {
        return this.current;
    }

    public onSearch?: (s: string) => void;

    private onDoIt(): void {
        if (this.onSearch) {
            this.onSearch(this.current);
        }
    }

    private current: string;
    private readonly active: boolean;
    private readonly cssBase: string;
    private lazy_element?: HTMLDivElement;
}

//------------------------------------------------------------------------------
// Entry Details
//------------------------------------------------------------------------------
export class EntryDetails  implements Control {
    // Control
    public render(): HTMLDivElement {
        if (!this.lazy_searchbar_holder) {
            this.lazy_searchbar_holder = du.div();
        }
        du.removeChildren(this.lazy_searchbar_holder);
        this.lazy_searchbar_holder.appendChild(this.searchBar.render());

        if (!this.is_dirty) {
            return this.lazy_element!;
        }

        if (!this.lazy_element || !this.lazy_properties) {
            this.lazy_element = du.div();
            addCssBase(this.lazy_element, this.cssBase, "_entryDetails");

            if (this.setTitle) {
                this.setTitle(this.id);
            } else {
                const eid = this.lazy_element.appendChild(du.div());
                addCssBase(eid, this.cssBase, "_entryDetails_id");
                eid.appendChild(du.text(this.id));
            }

            this.lazy_element.appendChild(this.lazy_searchbar_holder);

            this.lazy_properties = this.lazy_element.appendChild(du.div());
            addCssBase(this.lazy_properties, this.cssBase, "_entryDetails_properties");

            /* new prop ui
            this.lazy_newproperty = du.div();

            const newProp = this.lazy_newproperty.appendChild(du.textBox());
            addCssBase(newProp, this.cssBase, "_entryDetails_newProp")

            newProp.onkeypress = (event: KeyboardEvent) => {
                if (event.key === "Enter") {
                    const newPropName = newProp.value.trim();
                    if (newPropName === "") {
                        return false;
                    }

                    const p = makeEntryProperty(newPropName);
                    p.sortPriority = this.properties.length;
                    this.addProperty(p);
                    this.render();

                    newProp.value = "";
                    newProp.focus();
                    return false;
                }
            };
            /**/

            this.lazy_element.appendChild(du.horizontalRule());
            const backlinks = this.lazy_element.appendChild(du.div());
            addCssBase(backlinks, this.cssBase, "_entryDetails_backlinks");

            const sortedBacklinks = this.backlinks.slice();
            sortedBacklinks.sort(elementSorter);
            for (let i = 0; i < sortedBacklinks.length; ++i) {
                const b = sortedBacklinks[i]!;

                const bn = backlinks.appendChild(du.div([du.text(`${b.display} of`)]));
                addCssBase(bn, this.cssBase, "_entryDetails_backlink_name");

                const bvs = backlinks.appendChild(du.div());
                addCssBase(bvs, this.cssBase, "_entryDetails_backlink_value");

                if (b.values) {
                    this.renderPropertyValues(b.values, bvs);
                }
            }
        }

        const eps = this.lazy_properties;
        du.removeChildren(eps);

        /* new prop ui
        eps.appendChild(this.lazy_newproperty!);
        /**/

        const sortedProps = this.properties.slice();
        sortedProps.sort(elementSorter);
        for (let i = 0; i < sortedProps.length; ++i) {
            const p = sortedProps[i]!;

            if (!p.lazy_elements) {
                p.lazy_elements = [];

                let render = true;
                if (p.customRenderer) {
                    const custom = p.customRenderer();
                    if (custom) {
                        p.lazy_values = du.div([custom]);
                        addCssBase(p.lazy_values, this.cssBase, "_entryDetails_property_value");
                        p.lazy_elements.push(p.lazy_values);
                        render = false;
                    }
                }

                if (render) {
                    /* don't render the delete button
                    p.lazy_button = du.button();
                    p.lazy_elements.push(du.div([p.lazy_button]));
                    /**/

                    const epn = du.div([du.text(p.display)]);
                    addCssBase(epn, this.cssBase, "_entryDetails_property_name");
                    p.lazy_elements.push(epn);

                    p.lazy_values = du.div();
                    addCssBase(p.lazy_values, this.cssBase, "_entryDetails_property_value");
                    p.lazy_elements.push(p.lazy_values);

                    this.renderPropertyValues(p.values, p.lazy_values);
                }

                for (const le of p.lazy_elements) {
                    addCssBase(le, this.cssBase, "_entryDetails_property");
                }
            }

            for (const le of p.lazy_elements) {
                eps.appendChild(le);

                if (p.uiState === "adding") {
                    p.uiState = "added";
                    le.classList.remove(`${this.cssBase}_entryDetails_property_removed`);
                    /* don't render the delete button
                    p.lazy_button!.innerText = "-";
                    p.lazy_button!.onclick = (ev: MouseEvent) => {
                        this.removeProperty(p);
                        this.render();
                    }
                    */
                } else if (p.uiState === "removing") {
                    p.uiState = "removed";
                    le.classList.add(`${this.cssBase}_entryDetails_property_removed`);
                    /* don't render the delete button
                    p.lazy_button!.innerText = "+";
                    p.lazy_button!.onclick = (ev: MouseEvent) => {
                        this.addProperty(p);
                        this.render();
                    }
                    */
                }
            }
        }

        // TODO first entry

        this.is_dirty = false;
        return this.lazy_element;
    }

    renderPropertyValues(values: PropertyValue[], slot: HTMLElement) {
        const sortedValues = values.slice();
        sortedValues.sort();
        for (const v of sortedValues) {
            /* don't do a text box
            const vbox = vdiv.appendChild(du.textBox(v.value));
            addCssBase(vbox, this.cssBase, "_entryDetails_property_value");
            */

            /*
            if (v.valueExtras.length > 0) {
                let extras = " ";
                for (const e of v.valueExtras) {
                    extras = extras.concat(`${e}, `);
                }
                slot.appendChild(du.text(`${v.value} (${extras})`));
            } else {
                slot.appendChild(du.text(v.value));
            }
            if (v.goToTarget) {
                slot.appendChild(du.text(" "));
                slot.appendChild(du.button("→", () => { v.goToTarget!(); }));
            }
            */
            let text;
            if (v.valueExtras.length > 0) {
                let extras = " ";
                for (const e of v.valueExtras) {
                    extras = extras.concat(`${e}, `);
                }
                text = `${v.value} (${extras})`;
            } else {
                text = v.value;
            }
            if (v.goToTarget) {
                slot.append(du.link(text, v.goToTarget));
            } else {
                slot.append(du.text(text));
            }
            slot.appendChild(du.newLine());

            if (v.tags.length > 0) {
                const sortedTags = v.tags.slice();
                sortedTags.slice();

                const ts = slot.appendChild(du.paragraph());
                addCssBase(ts, this.cssBase, "_entryDetails_property_value_indent");
                for (const t of sortedTags) {
                    ts.appendChild(du.text(`${t}, `));
                }
            }
        }
    }

    public constructor(id: string, searchable: boolean, initial: string, cssBase: string) {
        this.searchBar = new SearchBar(initial, searchable, `${cssBase}_search`);
        this.id = id;
        this.properties = [];
        this.backlinks = [];
        this.cssBase = cssBase;
        this.is_dirty = true;
    }

    public addProperty(p: Property): void {
        p.uiState = "adding";
        if (this.properties.indexOf(p) < 0) {
            this.properties.push(p);
        }

        if (this.onPropertyChanged) {
            this.onPropertyChanged("added", p);
        }

        this.is_dirty = true;
    }

    public removeProperty(p: Property): void {
        p.uiState = "removing";

        if (this.onPropertyChanged) {
            this.onPropertyChanged("removed", p);
        }

        this.is_dirty = true;
    }

    public addBacklink(p: Property): void {
        p.uiState = "added";
        this.backlinks.push(p);
    }

    public onPropertyChanged?: (change: "added" | "removed" | "valueChanged", property: Property) => void;
    public setTitle?: (display: string) => void;

    public readonly searchBar: SearchBar;

    private readonly id: string;
    private readonly properties: Property[];
    private readonly backlinks: Property[];

    private readonly cssBase: string;
    private lazy_element?: HTMLDivElement;
    private is_dirty: boolean;

    private lazy_properties?: HTMLDivElement;
    private lazy_newproperty?: HTMLDivElement;
    private lazy_searchbar_holder?: HTMLDivElement;
}

export interface Property extends Sortable {
    display: string,
    sortPriority: number,
    values: PropertyValue[],

    customRenderer?: CustomRenderer,

    uiState: "adding" | "added" | "removing" | "removed";
    lazy_elements?: HTMLDivElement[];
    lazy_button?: HTMLButtonElement;
    lazy_values?: HTMLDivElement;
}

export interface PropertyValue {
    value: string,
    valueExtras: string[],
    tags: string[],
    goToTarget?: () => void,
}

export type CustomRenderer = () => HTMLElement | null;

export function makeMainEntryDetails(id: string, initial: string, cssBase: string): EntryDetails {
    return new EntryDetails(id, true, initial, cssBase);
}

export function makePinnedEntryDetails(id: string, initial: string, cssBase: string): EntryDetails {
    return new EntryDetails(id, false, initial, cssBase);
}

export function makeEntryProperty(display: string): Property {
    return { display: display, sortPriority: 0, values: [], uiState: "removed" };
}

export function makePropertyValue(v: string): PropertyValue {
    return { value: v, valueExtras: [], tags: [] };
}

//------------------------------------------------------------------------------
// Entry List
//------------------------------------------------------------------------------
export class EntryList implements Control {
    // Control
    public render(): HTMLDivElement {
        if (!this.lazy_searchbar_holder) {
            this.lazy_searchbar_holder = du.div();
        }
        du.removeChildren(this.lazy_searchbar_holder);
        this.lazy_searchbar_holder.appendChild(this.searchBar.render());

        if (!this.is_dirty) {
            return this.lazy_element!;
        }

        if (!this.lazy_element || !this.lazy_entries) {
            this.lazy_element = du.div();
            addCssBase(this.lazy_element, this.cssBase, "_entryDetails");

            if (this.setTitle) {
                this.setTitle(this.initial);
            }

            this.lazy_element.appendChild(this.lazy_searchbar_holder);

            this.lazy_entries = this.lazy_element.appendChild(du.div());
            addCssBase(this.lazy_entries, this.cssBase, "_entryDetails_list");
        }

        const es = this.lazy_entries;
        du.removeChildren(es);

        const sortedEntities = this.entities.slice();
        sortedEntities.sort(elementSorter);
        for (let i = 0; i < sortedEntities.length; ++i) {
            const e = sortedEntities[i]!;

            if (!e.lazy_elements) {
                e.lazy_elements = [];

                const en = du.div();
                addCssBase(en, this.cssBase, "_entryDetails_property_name");
                e.lazy_elements.push(en);

                if (e.goToTarget) {
                    en.append(du.link(e.display, e.goToTarget));
                } else {
                    en.append(du.text(e.display));
                }

                const eks = du.div();
                addCssBase(eks, this.cssBase, "_entryDetails_property_value");
                e.lazy_elements.push(eks);

                this.renderEntryValues(e.values, eks);

                for (const le of e.lazy_elements) {
                    addCssBase(le, this.cssBase, "_entryDetails_property");
                }
            }

            for (const le of e.lazy_elements) {
                es.appendChild(le);
            }
        }

        this.is_dirty = false;
        return this.lazy_element;
    }

    renderEntryValues(values: PropertyValue[], slot: HTMLElement) {
        const sortedValues = values.slice();
        sortedValues.sort();
        for (const v of sortedValues) {
            let text;
            if (v.valueExtras.length > 0) {
                let extras = " ";
                for (const e of v.valueExtras) {
                    extras = extras.concat(`${e}, `);
                }
                text = `${v.value} (${extras})`;
            } else {
                text = v.value;
            }
            if (v.goToTarget) {
                slot.append(du.link(text, v.goToTarget));
            } else {
                slot.append(du.text(text));
            }
            slot.appendChild(du.newLine());

            if (v.tags.length > 0) {
                const sortedTags = v.tags.slice();
                sortedTags.slice();

                const ts = slot.appendChild(du.paragraph());
                addCssBase(ts, this.cssBase, "_entryDetails_property_value_indent");
                for (const t of sortedTags) {
                    ts.appendChild(du.text(`${t}, `));
                }
            }
        }
    }

    public constructor(searchable: boolean, initial: string, cssBase: string) {
        this.searchBar = new SearchBar(initial, searchable, `${cssBase}_search`);
        this.initial = initial;
        this.entities = [];
        this.cssBase = cssBase;
        this.is_dirty = true;
    }

    public addEntry(e: EntrySummary): void {
        if (this.entities.indexOf(e) < 0) {
            this.entities.push(e);
        }

        this.is_dirty = true;
    }
    public setTitle?: (display: string) => void;

    public readonly searchBar: SearchBar;
    private readonly initial: string;
    private readonly entities: EntrySummary[];

    private readonly cssBase: string;
    private lazy_element?: HTMLDivElement;
    private lazy_entries?: HTMLDivElement;
    private is_dirty: boolean;
    private lazy_searchbar_holder?: HTMLDivElement;
}

export interface EntrySummary extends Sortable {
    display: string,
    sortPriority: number,
    values: PropertyValue[],
    goToTarget?: () => void,

    lazy_elements?: HTMLDivElement[];
}

export function makeMainEntryList(initial: string, cssBase: string): EntryList {
    return new EntryList(true, initial, cssBase);
}

export function makePinnedEntryList(initial: string, cssBase: string): EntryList {
    return new EntryList(false, initial, cssBase);
}

export function makeEntrySummary(display: string): EntrySummary {
    return { display: display, sortPriority: 0, values: [] };
}

//------------------------------------------------------------------------------
// Navigator
//------------------------------------------------------------------------------
export class Navigator implements Control {
    // Control
    public render(): HTMLDivElement {
        if (!this.isDirty) {
            return this.lazyElement!;
        }

        if (!this.lazyElement) {
            this.lazyElement = du.div();
            addCssBase(this.lazyElement, this.cssBase, "_all");

            this.lazySelector = this.lazyElement.appendChild(du.select());
            addCssBase(this.lazySelector, this.cssBase, "_selector");
            this.lazySelector.onchange = () => {
                this.selectedDocument = this.lazySelector!.selectedIndex;
                if (this.onDocumentSelected) {
                    this.onDocumentSelected(this.documents[this.selectedDocument]!);
                }
            }

            const back = this.lazyElement.appendChild(du.button("<", () => {
                if (this.onBack) {
                    this.onBack();
                }
            }));
            addCssBase(back, this.cssBase, "_back");

            const forward = this.lazyElement.appendChild(du.button(">", () => {
                if (this.onForward) {
                    this.onForward();
                }
            }));
            addCssBase(forward, this.cssBase, "_forward");
        }

        const sel = this.lazySelector!;
        du.removeChildren(sel);

        // TODO sort documents?

        for (const d of this.documents) {
            sel.appendChild(du.option(d));
        }
        sel.selectedIndex = this.selectedDocument;

        this.isDirty = false;
        return this.lazyElement;
    }

    public constructor(documents: string[], selected: number, cssBase: string) {
        this.documents = documents;
        this.selectedDocument = selected;
        this.cssBase = cssBase;
        this.isDirty = true;
    }

    public addDocument(display: string, select?: boolean): void {
        this.documents.push(display);
        this.isDirty = true;

        if (select) {
            this.selectedDocument = this.documents.length - 1;
        }
    }

    public onDocumentSelected?: (name: string) => void;
    public onBack?: () => void;
    public onForward?: () => void;

    private readonly documents: string[];
    private selectedDocument: number;
    private readonly cssBase: string;

    private isDirty: boolean;
    private lazyElement?: HTMLDivElement;
    private lazySelector?: HTMLSelectElement;
}

export function makeNavigator(documents: string[], selected: number, cssBase: string): Navigator {
    return new Navigator(documents, selected, cssBase);
}

//------------------------------------------------------------------------------
// TabBar
//------------------------------------------------------------------------------

export class TabBar implements Control {
    // Control
    public render(): HTMLDivElement {
        if (!this.isDirty) {
            return this.lazyElement!;
        }

        if (!this.lazyElement) {
            this.lazyElement = du.div();

            addCssBase(this.lazyElement, this.cssBase, "_all");
        }

        const ts = this.lazyElement!;
        du.removeChildren(ts);

        const mainTab = this.tabs[0];

        const sortedTabs = this.tabs.slice();
        sortedTabs.sort(elementSorter);
        for (let i = 0; i < sortedTabs.length; ++i) {
            let t = sortedTabs[i]!;

            if (!t.lazy_element) {
                t.lazy_element = du.div();
                t.lazy_element.appendChild(du.text(`${t.display} `));
                if (t === mainTab) {
                    t.lazy_element.appendChild(du.button("📌", () => { this.pin(t); }));
                } else {
                    t.lazy_element.appendChild(du.button("X", () => { this.unpin(t); }));
                }
                addCssBase(t.lazy_element, this.cssBase, mainTab ? "_main" : "_pinned");
                t.lazy_element.onclick = (ev: MouseEvent) => {
                    if (t.lazy_element !== ev.target) {
                        return; // this event is bubbling up from the button of a tab that has been removed
                    }
                    this.selectTab(t, false);
                }
            }

            if (t === this.selectedTab) {
                addCssBase(t.lazy_element, this.cssBase, "_selected");
            } else {
                removeCssBase(t.lazy_element, this.cssBase, "_selected");
            }

            ts.appendChild(t.lazy_element);
        }

        // add one more entry to have right hand side padding
        let pad = ts.appendChild(du.div());

        this.isDirty = false;

        return this.lazyElement;
    }

    public constructor(tab: Tab, cssBase: string) {
        let te = { item: tab.item, display: tab.display, sortPriority: -1 };
        this.tabs = [te];
        this.selectedTab = this.tabs[0]!;
        this.cssBase = cssBase;
        this.isDirty = true;
    }

    public updateMainTab(tab: Tab): void {
        let tabEntry = this.tabs[0]!;
        let sortPriority = tabEntry.sortPriority;
        rebindTab(tabEntry, tab);
        tabEntry.sortPriority = sortPriority; // client cannot change the priority of the main tab
        this.isDirty = true;

        this.render();
    }
    public addTab(t: Tab) {
        let newTab = { item: t.item, display: t.display, sortPriority: 0 };
        this.tabs.push(newTab);

        this.isDirty = true;
    }

    private pin(t: TabEntry): void {
        this.addTab(t);

        if (this.onPin) {
            this.onPin(t, this.tabs.length - 2); // don't count main tab
        }

        this.render();
    }
    private unpin(t: TabEntry): void {
        let i = this.tabs.indexOf(t);
        this.tabs.splice(i, 1);

        if (this.onUnpin) {
            this.onUnpin(t, i - 1); // don't count main tab
        }

        this.selectTab(this.tabs[0]!, true);
    }

    public selectMainTab(): void {
        this.selectTab(this.tabs[0]!, false);
    }
    private selectTab(t: TabEntry, forceRender: boolean): void {
        if (this.selectedTab === t && !forceRender) {
            return;
        }

        if (this.onTabSelected) {
            this.onTabSelected(t, t !== this.tabs[0]);
        }

        this.selectedTab = t;

        this.isDirty = true;
        this.render();
    }

    public onTabSelected?: (tab: Tab, pinned: boolean) => void;

    public onPin?: (tab: Tab, index: number) => void;
    public onUnpin?: (tab: Tab, index: number) => void;

    private readonly tabs: TabEntry[];
    private selectedTab: TabEntry;
    private readonly cssBase: string;

    private isDirty: boolean;
    private lazyElement?: HTMLDivElement;
}

export function makeTabBar(initialTab: Tab, cssBase: string): TabBar {
    return new TabBar(initialTab, cssBase);
}

export interface Tab {
    item: unknown;
    display: string;
}

export function makeTab(item: unknown, display: string): Tab {
    return { item: item, display: display };
}

interface TabEntry extends Tab { // Sortable
    sortPriority: number,
    lazy_element?: HTMLDivElement,
}

function rebindTab(tab: TabEntry, newTab: Tab): void {
    tab.item = newTab.item;
    tab.display = newTab.display;
    tab.lazy_element = undefined;
}

//------------------------------------------------------------------------------
// Nested List
//------------------------------------------------------------------------------
export interface NestedList {
    items: ListEntry[],
    lazy_element?: HTMLUListElement,
}

export interface ListEntry extends Sortable {
    item: unknown,

    display: string,
    sortPriority: number,

    children?: NestedList,
    lazy_element?: HTMLLIElement,
    click_handler?: (i: ListEntry) => void,
}

export function makeNestedList(parent?: ListEntry): NestedList {
    const l = { items: [] };
    if (parent) {
        parent.children = l;
    }
    return l;
}

export function makeListItem(item: unknown, display: string, parent: NestedList): ListEntry {
    const i = { item: item, display: display, sortPriority: 0 };
    parent.items.push(i);
    return i;
}

export function setListClickHandler(list: NestedList, handler: (i: ListEntry) => void): void {
    for (const c of list.items) {
        c.click_handler = handler;

        if (c.children) {
            setListClickHandler(c.children, handler);
        }
    }
}

export function renderNestedList(list: NestedList, cssBase: string, cssClasses?: string[]): HTMLUListElement {
    if (!list.lazy_element) {
        list.lazy_element = du.list(cssClasses);
        list.lazy_element.classList.add(`${cssBase}_ul`);
    }
    const e = list.lazy_element;

    const items = list.items.slice();
    items.sort(elementSorter);

    for (const i of items) {
        if (!i.lazy_element) {
            i.lazy_element = du.listElement(cssClasses);
            i.lazy_element.classList.add(`${cssBase}_li`);

            const ic = i.lazy_element.appendChild(du.div());
            let icon: HTMLSpanElement;
            if (i.children) {
                icon = ic.appendChild(du.text("-"));
                addCssBase(icon, cssBase, "_li_icon");
            }
            let text = ic.appendChild(du.text(i.display));
            if (i.children) {
                addCssBase(text, cssBase, "_li_heading");
            }
            ic.onclick = () => {
                if (i.children && i.children.lazy_element) {
                    if (i.children.lazy_element.hidden) {
                        icon.innerText = `-`;
                    } else {
                        icon.innerText = `+`;
                    }
                    i.children.lazy_element.hidden = !i.children.lazy_element.hidden;
                } else if (i.click_handler) {
                    i.click_handler(i);
                }
            }
        }
        const ie = i.lazy_element;

        if (i.children) {
            const subList = renderNestedList(i.children, cssBase, cssClasses);
            ie.appendChild(subList);
        }

        e.appendChild(ie);
    }

    return e;
}

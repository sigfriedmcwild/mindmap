import * as src from "./engine/search.js";

// TODO support storing history?

export interface Item
{
    display: string;
    rawQuery: string;
    parsedQuery: src.ParseResult;
}

export function makeItemShowAll(): Item {
    return makeItemFromQuery("id+");
}

export function makeItemFromId(id: string): Item {
    return { display: id, rawQuery: `id = ${id}`, parsedQuery: src.makeId(id) };
}

export function makeItemFromQuery(query: string): Item {
    return { display: query, rawQuery: query, parsedQuery: src.parse(query) };
}

function restoreItem(i: Item): void {
    i.parsedQuery = src.parse(i.rawQuery);
}

export interface Data {
    currentDocument?: string;
    documents: Documents;
}

export interface Documents {
    [k: string]: DocumentTabs;
};

export interface DocumentTabs {
    main: Item;
    pinned: Item[];
}

export class State {
    public constructor() {
        let save = localStorage.getItem(STORAGE_KEY);

        if (save) {
            const partialData = JSON.parse(save) as Data; // items may not have parsed queries right now!
            for (const k of Object.keys(partialData.documents)) {
                const doc = partialData.documents[k]!;
                restoreItem(doc.main);
                for (const i of doc.pinned) {
                    restoreItem(i);
                }
            }
            this.data = partialData;
        } else {
            this.data = { documents: {} };
        }
    }

    public Document(): string | undefined {
        return this.data.currentDocument;
    }

    public Tabs(): Item[] {
        let document = this.currentDocument();
        return [document.main, ...document.pinned];
    }

    public onNavigate(e: Item): void {
        let document = this.currentDocument();
        document.main = e;

        console.log(`onNavigate(${e ? JSON.stringify(e) : "<home>"})`);
        this.updateSave();
    }
    public onPin(e: Item, index: number): void {
        let document = this.currentDocument();
        document.pinned.splice(index, 0, e);

        console.log(`onPin(${e ? JSON.stringify(e) : "<home>"}, ${index})`);
        this.updateSave();
    }
    public onUnpin(e: Item, index: number): void {
        let document = this.currentDocument();
        document.pinned.splice(index, 1);

        console.log(`onUnpin(${e ? JSON.stringify(e) : "<home>"}, ${index})`);
        this.updateSave();
    }
    public onDocumentSelected(name: string): void {
        this.data.currentDocument = name;

        if (!this.data.documents[name]) {
            this.data.documents[name] = { main: makeItemShowAll(), pinned: [] };
        }

        console.log(`onDocumentSelected(${name})`);
        this.updateSave();
    }

    private currentDocument(): DocumentTabs {
        if (!this.data.currentDocument) {
            throw new Error("Current document is not set");
        }

        let document = this.data.documents[this.data.currentDocument];
        if (!document) {
            throw new Error("Current document is invalid");
        }
        return document;
    }

    private updateSave(): void {
        let save = JSON.stringify(this.data);
        console.log(save);
        localStorage.setItem(STORAGE_KEY, save);
    }

    private data: Data;
}

const STORAGE_KEY = "uiState";
